package com.kirillk.scrapper;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DouVacanciesScrapper {
  int countData = 0;

  public List<Document> scrap(String url, Map<String, String> headers, Token token) {
    List<Document> documents = new ArrayList<>();
    Connection.Response response = null;
    do {
      JsoupConnection jconnection = new JsoupConnection();
      Connection connection = jconnection.postConnection(url, headers, token, countData);
      try {
        response = connection.execute();
      } catch (IOException e) {
        e.printStackTrace();
      }
      countData += 40;
      documents.add(Jsoup.parse(new Gson().fromJson(response.body(), Map.class).get("html").toString()));
    } while (!(Boolean) new Gson().fromJson(response.body(), Map.class).get("last"));
    return documents;
  }

  public Document scrap(String url) {
    JsoupConnection jconnection = new JsoupConnection();
    Connection connection = jconnection.getConnection(url);
    Document result = null;
    try {
      result = connection.execute().parse();
    } catch (IOException e) {
      e.printStackTrace();
    }

    return result;
  }
}
