package com.kirillk.scrapper;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class DouVacanciesParser {
  private final String vacanciesFromGetQuery = "#vacancyListId .vacancy";
  private final String vacanciesFromPostQuery = ".vacancy";
  private final String jobTitleQuery = "#vacancyListId .vacancy  .title .vt";
  private final String companyTitleQuery = "#vacancyListId .vacancy  .title .company";
  private final String companyLogoQuery = "#vacancyListId .vacancy  .title .company img.f-i";
  private final String citiesQuery = "#vacancyListId .vacancy  .title .cities";
  private final String summaryQuery = "#vacancyListId .vacancy  .sh-info";


  public Set<Vacancy> parse(Document document) {
    Elements vacancies = document.select(vacanciesFromGetQuery);
    if (vacancies.isEmpty()) {
      vacancies = document.select(vacanciesFromPostQuery);
    }
    return mapVacancies(vacancies);
  }

  public Set<Vacancy> parse(List<Document> documents) {
    Set<Vacancy> vacancies = new HashSet<>();
    for (Document d: documents) {
      Elements e = d.select(vacanciesFromGetQuery);
      if (e.isEmpty()) {
        e = d.select(vacanciesFromPostQuery);
      }
      vacancies.addAll(mapVacancies(e));
    }
    return vacancies;
  }

  private Set<Vacancy> mapVacancies(Elements vacancies) {
    Set<Vacancy> resultVacancies = new HashSet<>();
    for (Element e : vacancies) {
      resultVacancies.add(new Vacancy(e.select(jobTitleQuery.replace(vacanciesFromGetQuery, "")).text(),
              e.select(jobTitleQuery.replace(vacanciesFromGetQuery, "")).attr("href"),
              e.select(companyTitleQuery.replace(vacanciesFromGetQuery, "")).text(),
              e.select(companyTitleQuery.replace(vacanciesFromGetQuery, "")).attr("href"),
              e.select(companyLogoQuery.replace(vacanciesFromGetQuery, "")).attr("src"),
              e.select(citiesQuery.replace(vacanciesFromGetQuery, "")).text(),
              e.select(summaryQuery.replace(vacanciesFromGetQuery, "")).text()));
    }
    return resultVacancies;
  }
}
