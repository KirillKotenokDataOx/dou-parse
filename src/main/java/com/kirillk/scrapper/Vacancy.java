package com.kirillk.scrapper;

public class Vacancy {
private String jobTitle;
private String jobLink;
private String companyName;
private String companyJobsList;
private String companyLogo;
private String jobCities;
private String jobSummary;

  public Vacancy(String jobTitle, String jobLink, String companyName, String companyJobsList, String companyLogo, String jobCities, String jobSummary) {
    this.jobTitle = jobTitle;
    this.jobLink = jobLink;
    this.companyName = companyName;
    this.companyJobsList = companyJobsList;
    this.companyLogo = companyLogo;
    this.jobCities = jobCities;
    this.jobSummary = jobSummary;
  }

  public String getJobTitle() {
    return jobTitle;
  }

  public void setJobTitle(String jobTitle) {
    this.jobTitle = jobTitle;
  }

  public String getJobLink() {
    return jobLink;
  }

  public void setJobLink(String jobLink) {
    this.jobLink = jobLink;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getCompanyJobsList() {
    return companyJobsList;
  }

  public void setCompanyJobsList(String companyJobsList) {
    this.companyJobsList = companyJobsList;
  }

  public String getCompanyLogo() {
    return companyLogo;
  }

  public void setCompanyLogo(String companyLogo) {
    this.companyLogo = companyLogo;
  }

  public String getJobCities() {
    return jobCities;
  }

  public void setJobCities(String jobCities) {
    this.jobCities = jobCities;
  }

  public String getJobSummary() {
    return jobSummary;
  }

  public void setJobSummary(String jobSummary) {
    this.jobSummary = jobSummary;
  }

  @Override
  public String toString() {
    return  "Job Title: " + jobTitle + '\n' +
            "Job Link: " + jobLink + '\n' +
            "Company Name: " + companyName + '\n' +
            "Company Jobs List: " + companyJobsList + '\n' +
            "Company Logo: " + companyLogo + '\n' +
            "Job Cities: " + jobCities + '\n' +
            "Job Summary: " + jobSummary + '\n';
  }
}
