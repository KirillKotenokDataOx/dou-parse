package com.kirillk.scrapper;

import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.util.Map;


public class JsoupConnection {
  private final String userAgent = "Chrome/89.0.4389.128";

  public Connection getConnection(String getUrl) {
    return Jsoup.connect(getUrl)
            .userAgent(userAgent)
            .method(Connection.Method.GET);
  }

  public Connection postConnection(String postUrl, Map<String, String> headers, Token token, int countData) {
    return Jsoup.connect(postUrl)
            .userAgent(userAgent)
            .headers(headers)
            .ignoreContentType(true)
            .cookies(token.getCsrfToken())
            .data(token.getCsrfMiddlewareToken())
            .data("count", String.valueOf(countData))
            .method(Connection.Method.POST);
  }
}

