package com.kirillk.scrapper;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Token {
  private Map<String, String> csrfToken;
  private Map<String, String> csrfMiddlewareToken;

  public void updateCsrfToken(String baseUrl) {
    String d = null;
    Connection.Response res = null;

    try {
      res = Jsoup.connect(baseUrl).method(Connection.Method.GET).execute();
      d = res.body();
    } catch (IOException e) {
      e.printStackTrace();
    }

    this.csrfToken = res.cookies();
    this.csrfMiddlewareToken = Map.of("csrfmiddlewaretoken", d.substring(d.indexOf("window.CSRF_TOKEN =") + 21, d.indexOf("window.USER_ID") - 5));
  }
}
