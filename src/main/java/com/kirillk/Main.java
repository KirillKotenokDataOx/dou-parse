package com.kirillk;

import com.kirillk.scrapper.*;
import org.jsoup.nodes.Document;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class Main {
  public static void main(String[] args) {
    int count = 0;
    DouVacanciesScrapper douVacanciesScrapper = new DouVacanciesScrapper();
    DouVacanciesParser douVacanciesParser = new DouVacanciesParser();

    Token token = new Token();
    token.updateCsrfToken("https://jobs.dou.ua/vacancies/?category=Java");

    List<Document> d = douVacanciesScrapper.scrap("https://jobs.dou.ua/vacancies/xhr-load/?category=Java", Map.of("Host", "jobs.dou.ua",
            "Origin", "https://jobs.dou.ua",
            "Referer", "https://jobs.dou.ua/vacancies/?category=Java"), token);

    Set<Vacancy> vacancies = douVacanciesParser.parse(d);


    for (Vacancy v : vacancies) {
      System.out.println(++count);
      System.out.println(v.toString());
    }
  }
}
